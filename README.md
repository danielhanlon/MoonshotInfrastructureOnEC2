## Aim

To automate the creation of a fully functioning moonshot infrastructure as a test or proof of concept.

Based on the instructions provided by JISC at
https://wiki.moonshot.ja.net/display/Moonshot/Installation+and+How-to+Guides

## Choices

* The test infrastructure is built on Virtual Machines (VMs)
* Four VMs are created in the Cloud in AWS EC2 - IDP, RP, SSHD and Client
* The test service is sshd
* An institutional LDAP service is assumed for authentication
* An institutional OpenVPN service is assume for connection to LDAP
* *Vagrant* is used to describe and create the VMs in EC2
* *Ansible* is used to configure the VMs
* The infrastructure is __not__ automatically joined to a *trust router* network

## Pre-requisities

* Unix-like launch environment from which to deploy the infrastructure
* AWS account with permission to create EC2 instances
* AWS Identity and Access Management (IAM) credential
* Elastic-IPs for the IDP, RP and SSHD, each with a DNS entry
* vagrant
* ansible
* git

## Deploying

### Initial setup

Download a copy of the repository to a work area on a unix-like launch machine.

e.g.
```bash
git clone git@gitlab.com:danielhanlon/MoonshotInfrastructureOnEC2.git -b release
```

Copy the example files provided, to create real versions:

```bash
cp vars.yml.example vars.yml
cp vpn_vars.yml.example vpn_vars.yml
cp ldap_vars.yml.example ldap_vars.yml
```

...and edit them to provide setting appropriate to your requirements.

**_The example files are heavily commented but please get in touch if it's unclear
what any of the options refers to._**

A couple of vagrant plugins must be installed with:

```bash
vagrant plugin install vagrant-aws
vagrant plugin install vagrant-reload
```

### Deploy the VMs

#### *IDP*

The IDP machine must be created before the others as it creates a Certificate
Authority and certificates for itself and the other machines.

```bash
vagrant up idp
```

...and go to have a cup of tea...

When the build finally completes, near the end of the output should be something like:

```
TASK [debug] *******************************************************************
ok: [idp] => {
    "test_result.stdout_lines": [
        "Sent Access-Request Id 255 from 0.0.0.0:42976 to 127.0.0.1:1812 length 96",
        "\tUser-Name = \"<vpn_username>@<realm>\"",
        "\tUser-Password = \"<vpn_password>\"",
        "\tNAS-IP-Address = 127.0.0.1",
        "\tNAS-Port = 2222",
        "\tMessage-Authenticator = 0x00",
        "\tCleartext-Password = \"<vpn_password>\"",
        "Received Access-Accept Id 255 from 127.0.0.1:1812 to 0.0.0.0:0 length 20"
    ]
    }
```

**_N.B. The assumption of this test is that the VPN credentials
(vpn_username and vpn_password defined in vpn_vars.yml) are valid credentials
for your LDAP server... If this is not the case then the test will fail._**

If you see the line ``Received Access-Accept``, three lines from the end then
you've been able to connect to your LDAP server via the VPN and authenticate
the VPN user. If you see ```Access-Reject`` or have other errors then these should be
corrected before continuing.

In the case of misconfiguration it is often best to destroy this VM
with ```vagrant destroy idp```, change the configuration and restart with
```vagrant up idp```.

#### *RP, SSHD* and *client*

The other machines can now be created with:

```bash
vagrant up rp
vagrant up sshd
vagrant up client
```

Assuming that all machines are created and configured successfully the infrastructure
is now complete, but it will not be aware of any user accounts.

### User creation

In order to test the process of logging in to the sshd service via moonshot, the
infrastructure and service need to know about the account that will be connecting.

1. The RP machine has a database mapping the moonshot targeted ID to an account
2. The account must exist on the sshd machine

The script ```userAdd``` takes an account name as a parameter. The account name
is assumed to exist in the LDAP that ultimately authenticates the request.

```bash
./userAdd <account>
```

and, all being well, the database mapping and account creation will take place.

### Testing the sshd service with Moonshot

*A working XServer allowing remote connections must be running on your* launch
*machine in order to complete this test.*

The ```clientTerminal``` script opens a terminal window XSession on the Client
test machine.

```bash
./clientTerminal <account>
```
The home directory contains an XML file describing your moonshot credentials
created by the ```userAdd``` step above.

These can be installed with:

```bash
moonshot-webp ./credential.xml
```

A GUI will appear asking for confirmation.

Once the credentials have been successfully installed, you're ready to use them
to connect to the moonshot-enabled sshd service.

The home directory contains a file ```connectToSSH``` that contains the
command to open the ssh connection.

When you run this, the moonshot identity selector will ask you to choose your
credential (there should only be the one that we just added). If you choose to
send it, you will be presented with a pop-up window asking for your password.

Entering the password will make the connection and should present you with
a prompt on your SSHD machine.

## Troubleshooting

Problems are often most easily investigated from the machines themselves.
Vagrant makes it easy to connect to the VMs with

```bash
vagrant ssh idp
```
```bash
vagrant ssh rp
```
or
```bash
vagrant ssh sshd
```

...so, for example, the VPN connection can be tested by connecting to the IDP
and attempting to debug the connection.

## Thankyou

The configuration and setup that automated here is based on following the
instructions provided by JISC at:

https://wiki.moonshot.ja.net/display/Moonshot/Installation+and+How-to+Guides

and wouldn't have been possible without the very helpful and patient answers
from Alejandro Perez-Mendez at JISC to my many questions regarding Moonshot.
