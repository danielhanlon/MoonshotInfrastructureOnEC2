## Gotchas

There are issues with overloaded terms. For the purpose of this test installation:

* Client = the end user who seeks to use a service
* SSHD = service the Client connects to

It is important to understand which pieces of functionality are provided by
which logical machine - IDP, RP or SSHD.

FreeRADIUS comes with a heavily commented default configuration, much of which is
not required here, but is /mostly/ left in place.

FreeRADIUS uses the concepts of "available" and "enabled" in
the configuration directory e.g. mods-available, mods-enabled. Components are
made active by creating a symlink in the "enabled" directory to the "available"
configuration.
