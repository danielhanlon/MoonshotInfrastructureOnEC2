require 'yaml'
vars = YAML::load(File.read("#{File.dirname(__FILE__)}/vars.yml"))

Vagrant.configure("2") do |config|
  #Defaults
  config.vm.box = "dummy"
  config.vm.synced_folder ".", "/vagrant", type: "rsync",
    rsync__exclude: ".git/"
  config.vm.provider :aws do |aws|
    aws.instance_type = vars["aws_instance_type"]
    aws.access_key_id = vars["aws_access_key_id"]
    aws.secret_access_key = vars["aws_secret_access_key"]
    aws.keypair_name = vars["aws_keypair_name"]
    aws.region = vars["aws_region"]
    aws.security_groups = [vars["aws_security_group"]]
    aws.ami = vars["aws_default_ami"]
  end
  config.ssh.username = "admin"
  config.ssh.private_key_path = vars["aws_private_key_path"]

  #Utils machine in AWS
  config.vm.define "utils" do |subconfig|
    subconfig.vm.hostname = "utils"
    subconfig.vm.provider :aws do |aws, override|
      aws.tags = {
        'Name' => 'Utils'
      }
    end

    subconfig.vm.provision :ansible do |ansible|
      ansible.playbook = "update.yml"
    end
  end

  #IDP configuration
  config.vm.define "idp" do |subconfig|
    subconfig.vm.hostname = vars["idp_host"]
    subconfig.vm.provider :aws do |aws, override|
      aws.tags = {
        'Name' => 'Moonshot_IDP'
      }
      aws.elastic_ip = vars["idp_ip"]
    end

    subconfig.vm.provision :ansible do |ansible|
      ansible.playbook = "update.yml"
    end
    subconfig.vm.provision :reload

    subconfig.vm.provision :ansible do |ansible|
      ansible.playbook = "idp.yml"
    end

    subconfig.vm.provision "idp_freeradius_test", type: "ansible" do |ansible|
      ansible.playbook = "idp_freeradius_test.yml"
    end

    subconfig.vm.provision "vpn", type: "ansible" do |ansible|
      ansible.playbook = "vpn.yml"
    end

    subconfig.vm.provision "ldap_test", type: "ansible" do |ansible|
      ansible.playbook = "ldap_test.yml"
    end

    subconfig.vm.provision "idp_freeradius_ldap", type: "ansible" do |ansible|
      ansible.playbook = "idp_freeradius_ldap.yml"
    end

    #subconfig.vm.provision "clients.conf", type: "ansible" do |ansible|
    #  ansible.playbook = "clients.conf.yml"
    #end
  end

  #RP configuration
  config.vm.define "rp" do |subconfig|
    subconfig.vm.hostname = vars["rp_host"]
    subconfig.vm.provider :aws do |aws, override|
      aws.tags = {
        'Name' => 'Moonshot_RP'
      }
      aws.elastic_ip = vars["rp_ip"]
    end

    subconfig.vm.provision :ansible do |ansible|
      ansible.playbook = "update.yml"
    end
    subconfig.vm.provision :reload

    subconfig.vm.provision "rp", type: "ansible" do |ansible|
      ansible.playbook = "rp.yml"
    end

    subconfig.vm.provision "rp_user_mapping", type: "ansible" do |ansible|
      ansible.playbook = "rp_user_mapping.yml"
    end
    #subconfig.vm.provision "rp_username_database", type: "ansible" do |ansible|
    #  ansible.playbook = "rp_username_database.yml"
    #end
  end

  #SSHD - moonshot client configuration
  config.vm.define "sshd" do |subconfig|
    subconfig.vm.hostname = vars["sshd_host"]
    subconfig.vm.provider :aws do |aws, override|
      aws.tags = {
        'Name' => 'Moonshot_sshd'
      }
      aws.elastic_ip = vars["sshd_ip"]
    end

    subconfig.vm.provision :ansible do |ansible|
      ansible.playbook = "update.yml"
    end
    subconfig.vm.provision :reload

    subconfig.vm.provision "sshd", type: "ansible" do |ansible|
      ansible.playbook = "sshd.yml"
    end

    #subconfig.vm.provision "build_moonshot_sshd", type: "ansible" do |ansible|
    #  ansible.playbook = "build_moonshot_sshd.yml"
    #end

    subconfig.vm.provision "sshd_freeradius_test", type: "ansible" do |ansible|
      ansible.playbook = "sshd_freeradius_test.yml"
    end
  end

  #SSH client - sshd client with moonshot identity selector configuration
  config.vm.define "client" do |subconfig|
    subconfig.vm.hostname = "client"
    subconfig.vm.provider :aws do |aws, override|
      aws.tags = {
        'Name' => 'Moonshot_enabled_ssh_client'
      }
    end
    subconfig.ssh.forward_x11 = true
    subconfig.vm.provision "ansible" do |ansible|
      #Dummy ansible provision to put client in inventory
      ansible.playbook = "update.yml"
    end
    subconfig.vm.provision "shell", inline: <<-SHELL
      echo "deb http://repository.project-moonshot.org/debian-moonshot jessie main" > /etc/apt/sources.list.d/moonshot.list
      apt-get update
      apt-get install -y xauth
      apt-get install -y rxvt
      apt-get install -y wget
      wget -O - http://repository.project-moonshot.org/key.gpg | apt-key add -
      apt-get update
      apt-get install -y moonshot-gss-eap moonshot-ui
      hostname client
    SHELL
  end
end
