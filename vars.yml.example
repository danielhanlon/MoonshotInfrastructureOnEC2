#Identity Realm for the infrastructure as to be registered with Assent
#(can use anything here for intial testing without trust router)
realm: e.g. moonshot-test.camford.ac.uk
# -> radtest on IDP in idp_freeradius_ldap.yml
# -> templates/abfab-tls.j2 -> /etc/freeradius/sites-available/abfab-tls on IDP and RP
# -> templates/idp_site.j2 -> /etc/freeradius/sites-available/<IDP> on RP
# -> templates/proxy.conf.j2 -> /etc/freeradius/proxy.conf on RP

#####################################################################
#Values used to create certificates for the infrastructure
#####################################################################
country: e.g. UK
state: e.g. Scotland
locality: e.g. Pwllheli
organization: e.g. Imperial College London
email: <your e-mail address>
# -> idp.yml generates a CA certificate, certificates for the IDP:
# -> /etc/freeradius/certs/ca.pem on IDP
# -> /etc/freeradius/certs/server.pem on IDP
# -> /etc/freeradius/certs/client.pem on IDP
# ...and also certificates for the RP and SSHD in certs/idp/etc/freeradius/certs
# ...for installation by rp.yml and sshd.yml:
# certs/idp/etc/freeradius/certs/ca.pem -> /etc/freeradius/certs/ca.pem on RP and SSHD
# certs/idp/etc/freeradius/certs/rp-server.pem -> /etc/freeradius/certs/server.pem on RP
# certs/idp/etc/freeradius/certs/rp-client.pem -> /etc/freeradius/certs/client.pem on RP
# certs/idp/etc/freeradius/certs/sshd-client.pem -> /etc/freeradius/certs/client.pem on SSHD
#
#Infrastructure components... see
#https://wiki.moonshot.ja.net/display/Moonshot/Overview+of+Moonshot+Components

#####################################################################
#IDP - Identity Provider config
#####################################################################
idp: <internal name for your IDP> e.g. moonshot_idp
# -> rp.yml, templates/moonshot_idp.j2
idp_host: e.g. moonshot-idp-test.cs.camford.ac.uk
idp_ip: <IP address of idp_host>
# -> idp.yml, rp.yml

#####################################################################
# RP - Relying Party config
#####################################################################
rp: rcs_rp
rp_host: e.g. moonshot-rp-test.physics.camford.ac.uk
rp_ip: <IP address of moonshot-rp-test.physics.camford.ac.uk>
rp_database: <location for sqlite database of user mappings> e.g. /tmp/freeradius.db

#####################################################################
# Client - Moonshot client
#####################################################################
sshd_host: moonshot-sshd-test.theology.camford.ac.uk
sshd_ip: <IP address of moonshot-sshd-test.theology.camford.ac.uk>

salt: <some random text to randomise your identity hashes e.g. my_salt>
secret: <a shared secret for radsec connections e.g. my_secret>

#####################################################################
# AWS config
#####################################################################
# The machine type to be created - recommend leave unchanged for POC
aws_instance_type: t2.micro

# The base image for the machine - changing this is likely to cause the build to fail
aws_default_ami: ami-11c57862

# The AWS region in which the machines should run - can leave unchanged
aws_region: eu-west-1

# The AWS account credentials - do /not/ make public
aws_access_key_id: xxxxxxxxxxxxxxxxxxxx
aws_secret_access_key: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

# The AWS IAM credential to be used to access EC2 instances
aws_keypair_name: e.g. my_moonshot_poc_iam_credential
aws_private_key_path: e.g. my_moonshot_poc_iam_credential_key.pem

# The EC2 security group i.e. firewall config
aws_security_group: e.g. my_security_group
